import os
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow
import acp_times
import config
from pymongo import MongoClient

app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
race = {
}

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")

    return flask.render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")

    return flask.render_template('404.html'), 404

@app.route('/display')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/_submit')
def _submit():
    if race.get('datetime'):
        itemdoc = race
        db.tododb.insert_one(itemdoc)
        race.clear()
        result = {'msg' : 'Success!'}
    else:
        result = {'msg': 'Nothing to submit!'}
    return flask.jsonify(result=result)

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist_km = request.args.get('brevet_dist_km', 200, type=int)
    datetime = request.args.get('datetime')
    if race.get('datetime', '') != datetime: 
        race.clear()
        race['datetime'] = datetime
        race['kms'] = []
        race['opentimes'] = []
        race['closetimes'] = []
    app.logger.debug('datetime: {}'.format(datetime))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, brevet_dist_km, arrow.get(datetime).isoformat())
    close_time = acp_times.close_time(km, brevet_dist_km, arrow.get(datetime).isoformat())
    race['kms'].append(km)
    race['opentimes'].append(open_time)
    race['closetimes'].append(close_time)
    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(host='0.0.0.0', debug=True)
